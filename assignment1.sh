#! /bin/bash

 #find all the files older than 5 days and zip them 
 find /home/ec2-user/bhargav -type f -mtime +5 -name "*log" -exec gzip {} \; 

#find all the zip files which are older than 10 days and delete them
find /home/ec2-user/bhargav  -mtime +10 -type f -name "*log.gz" -exec rm {} \;


